" PLUGINS

call plug#begin('~/.vim/plugged')

" syntax and colors
Plug 'mhartington/oceanic-next'
Plug 'plasticboy/vim-markdown'
Plug 'sheerun/vim-polyglot'

" other
Plug 'justinmk/vim-ipmotion' " improved paragraph
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-sleuth'
Plug 'wellle/targets.vim' " additional text targets
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'ctrlpvim/ctrlp.vim'
" Plug 'heavenshell/vim-jsdoc'
Plug 'w0rp/ale'
Plug 'rking/ag.vim'
Plug 'simnalamburt/vim-mundo'
" Plug 'ternjs/tern_for_vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
"

call plug#end()

" LEADER

let mapleader=" "

" AUTOCOMMANDS
" wrap markdown files
au BufRead,BufNewFile *.md setlocal wrap
au BufRead,BufNewFile *.md setlocal linebreak
au! BufNewFile,BufRead *.svelte set ft=html
autocmd BufRead,BufNewFile *.md,*.mdx setlocal spell

" MISC SETTINGS

let g:deoplete#enable_at_startup = 1
let g:jsx_ext_required = 0
let g:mundo_prefer_python3 = 1
let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro'
let g:ale_linters = { 'javascript': ['eslint'], 'html': ['htmlhint', 'proselint'] }
let g:ale_fixers = { 'javascript': ['eslint'] }
let g:ale_fix_on_save = 1
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0

filetype plugin indent on
syntax enable

set autoindent
"set autochdir
set background=dark
set backspace=indent,eol,start
set clipboard=unnamed
set cul
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,tmp
set hidden
set history=10000
set hlsearch
set ignorecase smartcase
set incsearch
set laststatus=2
set list
set listchars=tab:·\ ,trail:·,eol:¬
set matchtime=1
set nobackup
set noswapfile
set nowrap
set nowritebackup
set number
set scrolloff=3
set shell=/bin/sh
set showmatch
set t_ti= t_te=
set timeout timeoutlen=1000 ttimeoutlen=100
set wildmenu
set wildmode=longest,list
set winwidth=79
set tabstop=2 shiftwidth=2 expandtab

" Status
set statusline=%t       "tail of the filename
set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=%y      "filetype
set statusline+=%=      "left/right separator
set statusline+=%c,     "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file

" UNDO
set undodir=~/.vim/undo
set undofile
set undolevels=1000
set undoreload=10000

" COLORS
colorscheme base16-colors
set background=dark


" KEY REMAPS

" buffer switching
nnoremap <c-j> :bprev<cr>
nnoremap <c-k> :bnext<cr>

" ctrlp 
nnoremap <c-p> :GitFiles<cr>

" turn off search highlighting
nnoremap \ :nohlsearch<cr>

" quick replace
nnoremap <leader>s :%s//g<left><left>

" quick replace
nnoremap <leader>j :JsDoc<cr>

" some handy shortcuts
nnoremap <leader>e :Ex<cr>
nnoremap <leader>w :w<cr>

" cursor movement in insert mode
imap <C-h> <Left>
imap <C-j> <Down>
imap <C-k> <Up>
imap <C-l> <Right>

" test
map <Leader>t :call RunCurrentSpecFile()<CR>

" toggle Mundo
nnoremap <F9> :MundoToggle<CR>

" deoplete tab completion
inoremap <silent><expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" if last buffer is destoryed, bring up netrw
nnoremap <leader>x :call KillOrNetrw()<cr>



" FUNCTIONS

" The Silver Searcher
if executable('ag')
    " Use ag over grep
    set grepprg=ag\ --nogroup\ --nocolor

    " Use ag in CtrlP for listing files. Lightning fast and respects ignore
    let g:ctrlp_user_command = 'ag %s -f -l --nocolor -g ""'

    " ag is fast enough that CtrlP doesn't need to cache
    let g:ctrlp_use_caching = 0

    " bind K to grep word under cursor
endif

" Open netrw if last buffer is killed
function! KillOrNetrw()
    if BufCount() == 1
        :Ex
        :call BufOnly()
    else
        :bd
    endif
endfunction

function! BufCount()
    "   return len(filter(range(1, bufnr('$')), 'buflisted(v:val)'))
endfunction

function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction

function! BufOnly()
    let buffer = bufnr('%')

    if buffer == -1
        echohl ErrorMsg
        echomsg "No matching buffer for" a:buffer
        echohl None
        return
    endif

    let last_buffer = bufnr('$')

    let delete_count = 0
    let n = 1
    while n <= last_buffer
        if n != buffer && buflisted(n)
            if getbufvar(n, '&modified')
                echohl ErrorMsg
                echomsg 'No write since last change for buffer'
                            \ n '(add ! to override)'
                echohl None
            else
                silent exe 'bdel' . '' . ' ' . n
                if ! buflisted(n)
                    let delete_count = delete_count+1
                endif
            endif
        endif
        let n = n+1
    endwhile

endfunction
