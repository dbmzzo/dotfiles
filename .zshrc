##############################################################################
# History Configuration
##############################################################################
HISTSIZE=5000               #How many lines of history to keep in memory
HISTFILE=~/.histfile        #Where to save history to disk
SAVEHIST=5000               #Number of history entries to save to disk
setopt    appendhistory     #Append history to the history file (no overwriting)
setopt    sharehistory      #Share history across terminals
setopt    incappendhistory  #Immediately append to the history file, not just when a term is killed

export TERM='xterm-256color'

# PROMPT AND COLORS
autoload -U colors && colors
export PS1="%{$fg[blue]%}[%m]%{$reset_color%}%{$fg[yellow]%}%(1j. [%j].)%{$reset_color%} %~ %{$fg[blue]%}>%{$reset_color%} "

# COMPLETTIONS
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' menu select
autoload -U compinit && compinit
autoload bashcompinit
bashcompinit

# PATH
export PATH="$PATH:/usr/local/share/npm/bin"
export PATH="/usr/local/bin::/usr/local/sbin::$PATH"
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="/opt/local/bin:/opt/local/sbin:/opt/local/include:$PATH"
export PATH="/Users/dbmzzo/Library/Python/3.6/bin:$PATH"
export PATH="/Users/dbmzzo/miniconda3/bin:$PATH"

# ALIASES
alias ls='ls -G'
alias ll='ls -al'
alias vim='nvim'
alias v='vim'
alias cl='clear'

## GIT
alias gs='git status'
alias ga.='git add . -A'
alias gp='git push'
alias gcm='git commit -m '
alias gb='git checkout -b'
alias gc='git checkout'

alias igpull='node index.js dbmzzo'
alias igpush='aws s3 sync dbmzzo s3://instagram-dbmzzo'
alias deploy_fe='npm run build ; aws s3 sync build s3://misc-print.com'

# MISC
setopt autocd
setopt correct

# RBENV
eval "$(rbenv init -)"

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/dbmzzo/.config/yarn/global/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/dbmzzo/.config/yarn/global/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`

[[ -f /Users/dbmzzo/.config/yarn/global/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/dbmzzo/.config/yarn/global/node_modules/tabtab/.completions/sls.zsh
export NVM_DIR="$HOME/.nvm"

if [ -s "$HOME/.nvm/nvm.sh" ] && [ ! "$(whence -w __init_nvm)" = function ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"
  declare -a __node_commands=('nvm' 'node' 'npm' 'yarn' 'gulp' 'grunt' 'webpack')
  function __init_nvm() {
    for i in "${__node_commands[@]}"; do unalias $i; done
    . "$NVM_DIR"/nvm.sh
    unset __node_commands
    unset -f __init_nvm
  }
  for i in "${__node_commands[@]}"; do alias $i='__init_nvm && '$i; done
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

precmd () {print -Pn "\e]2; %~/ \a"}
preexec () {print -Pn "\e]2; %~/ \a"}
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"

# added by travis gem
[ -f /Users/dbmzzo/.travis/travis.sh ] && source /Users/dbmzzo/.travis/travis.sh
